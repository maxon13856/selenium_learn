from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys # Позволяет что-то печатать в полях ввода
import time  # Позволяет задать время ожидания

PATH = "C:/Program Files (x86)/chromedriver.exe"
driver = webdriver.Chrome(PATH)
driver.get("http://appserver4.luxbase.int:5400")

# print(driver.title)  # Печатает в консоль название таба

# Логин в систему
try:
    logpass = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located(
            (By.XPATH, "/html/body/app-root/app-login/div/div/div[1]/div[2]/form/div[1]/app-text/label/div[1]/input"))
    )
    logpass = driver.find_element_by_xpath(
        "/html/body/app-root/app-login/div/div/div[1]/div[2]/form/div[1]/app-text/label/div[1]/input")  # Находим поле логина
    logpass.clear()
    logpass.send_keys("mvedeneev")  # Вводим логин
    logpass = driver.find_element_by_xpath(
        "/html/body/app-root/app-login/div/div/div[1]/div[2]/form/div[2]/app-password-input/label/span/input")  # Находим поле пароля
    logpass.send_keys("spring")  # Вводим пароль
    logpass.send_keys(Keys.RETURN)  # Нажимаем Enter
except:
    driver.quit()

# Вывожу весь текст из родителького элемента с дашбордов
# try:
#     main = WebDriverWait(driver, 10).until(
#         EC.presence_of_element_located((By.XPATH,"/html/body/app-root/app-layout/div/div/div/div/div/div/as-split/as-split-area/app-home/app-dashboard/div/div[2]"))
#     )
#     print(main.text)

# except:
#     driver.quit()

# Перехожу в константы
try:
    link = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located(
            (By.XPATH, "/html/body/app-root/app-layout/div/div/app-sidebar/nav/ul/perfect-scrollbar/div/div[1]/app-layout-menu/mat-nav-list/mat-list-item[2]/div/div[3]/mat-icon"))
    )
    link.click()
    link = driver.find_element_by_xpath(
        "/html/body/app-root/app-layout/div/div/app-sidebar/nav/ul/perfect-scrollbar/div/div[1]/app-layout-menu/mat-nav-list/div/mat-list-item[4]/div/div[3]/span")
    link.click()
except:
    driver.quit()

# driver.back() #Вернусь на предыдущую веб-страницу
# driver.forward() #Вернусь вперёд


time.sleep(10)
# driver.close() - закрыть таб
driver.quit()  # - закрыть окно
